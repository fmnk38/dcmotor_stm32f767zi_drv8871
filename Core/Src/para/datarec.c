#include <stdint.h>
#include <math.h>

#include "datarec.h"
#include "main.h"

#include "parameter.h"
#include "fifoparameter.h"

//#define NOOFCHANNELS 4
//#define BUFFERSIZE   4096  //total Size for all Channels
//static float databuf[BUFFERSIZE];

static float val_datarec_current[4096];
static parameter_t datarec_para_current = {
		.type = parameterType_float32,
		.name = "Current",
		.cnt = 4096,
		.pdata = val_datarec_current
};

static fifosupport_t datarec_support_current = {
		.size = 4096,
		.pread = 0,
		.pwrite = 0,
		.numlock = 0
};

fifoparameter_t datarec_current = {
		.parameter = &datarec_para_current,
		.fifosupport = &datarec_support_current
};


static float val_datarec_outpvolt[4096];
static parameter_t datarec_para_outpvolt = {
		.type = parameterType_float32,
		.name = "Output Voltage",
		.cnt = 4096,
		.pdata = val_datarec_outpvolt
};

static fifosupport_t datarec_support_outpvolt = {
		.size = 4096,
		.pread = 0,
		.pwrite = 0,
		.numlock = 0
};

fifoparameter_t datarec_outpvolt = {
		.parameter = &datarec_para_outpvolt,
		.fifosupport = &datarec_support_outpvolt
};


static float val_datarec_spd[4096];
static parameter_t datarec_para_spd = {
		.type = parameterType_float32,
		.name = "Speed",
		.cnt = 4096,
		.pdata = val_datarec_spd
};

static fifosupport_t datarec_support_spd = {
		.size = 4096,
		.pread = 0,
		.pwrite = 0,
		.numlock = 0
};

fifoparameter_t datarec_spd = {
		.parameter = &datarec_para_spd,
		.fifosupport = &datarec_support_spd
};


extern ctrl_data_t myctrl;

//one step (in real-time context)
void datarec(void){
	size_t idx;

	int ret = fifosupport_getIdxWrite(&datarec_support_current, &idx);
	if(!ret){
		val_datarec_current[idx] = myctrl.ifval;
		fifosupport_commitWrite(&datarec_support_current);
	}

	ret = fifosupport_getIdxWrite(&datarec_support_outpvolt, &idx);
	if(!ret){
		val_datarec_outpvolt[idx] = myctrl.uref;
		fifosupport_commitWrite(&datarec_support_outpvolt);
	}

	ret = fifosupport_getIdxWrite(&datarec_support_spd, &idx);
	if(!ret){
		val_datarec_spd[idx] = myctrl.speed_filt;
		fifosupport_commitWrite(&datarec_support_spd);
	}
}


//initialization
int datarec_init(void){
	return(0);
}

