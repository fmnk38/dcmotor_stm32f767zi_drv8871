#include "parameter.h"
#include "fifosupport.h"

#include "fifoparameter.h"

//size_t fifoparameter_numele(fifoparameter_t* para){
//	if(!para) return(0); //"0" means zero elements or parameter not available
//	return(fifosupport_numele(para->fifosupport));
//}

//Get number of elements available; also lock current numbers of elements for array read
size_t fifoparameter_numele(fifoparameter_t* para){
	if(!para) return(0); //"0" means zero elements or parameter not available
	return(fifosupport_lock(para->fifosupport));
}

int fifoparameter_toString(char* s, fifoparameter_t* para, size_t element){
	if(!para) return(ERR_NOPARA);
	size_t idx;
	int ret = fifosupport_getIdxLockRead(para->fifosupport, &idx, element);
	if(ret) return(ERR_OUTOFRANGE);

	ret = parameter_toString(s, para->parameter, idx);

	return(ret);
}

int fifoparameter_freeLocked(fifoparameter_t* para){
	return(fifosupport_freeLocked(para->fifosupport));
}

int fifoparameter_listGetPara(fifoparameter_t** para, size_t idx, fifoparameterList_t* paralist){
	if(idx >= paralist->cnt) return(ERR_OUTOFRANGE);   //index out of range
	fifoparameter_t* myPara = paralist->parameters[idx];
	*para = myPara;
	if(myPara == 0) return(ERR_NOPARA);   //parameter not available
	return(ERR_NONE);
}

int fifoparameter_listParaGetName(char** s, fifoparameterList_t* paralist, size_t idx){
	fifoparameter_t* para;
	int ret = fifoparameter_listGetPara(&para, idx, paralist);
	if(ret) return(ret);
	return(parameter_getName(s, para->parameter));
}


size_t fifoparameter_listParaNumele(fifoparameterList_t* paralist, size_t idx){
	fifoparameter_t* para;
	int ret = fifoparameter_listGetPara(&para, idx, paralist);
	if(ret) return(ret);
	return(fifoparameter_numele(para));
}

int fifoparameter_listParaToString(char* s, fifoparameterList_t* paralist, size_t idx, size_t element){
	fifoparameter_t* para;
	int ret = fifoparameter_listGetPara(&para, idx, paralist);
	if(ret) return(ret);
	ret = fifoparameter_toString(s, para, element);
	return(ret);
}

int fifoparameter_listParaFreeLocked(fifoparameterList_t* paralist, size_t idx){
	fifoparameter_t* para;
	int ret = fifoparameter_listGetPara(&para, idx, paralist);
	if(ret) return(ret);
	return(fifoparameter_freeLocked(para));
}
