#ifndef __FIFOSUPPORT_H
#define __FIFOSUPPORT_H

#include <stdlib.h>

//Attention! Access to pread, pwrite must be atomic on the platform! otherwise, use a lock on these variables!!
typedef struct {
  size_t size;   // numer of elements
  size_t pread;  // points to oldest element (next element to be read)
  size_t pwrite; // points to next element to be written
  size_t numlock; // number of elements locked to be read (for array read!)
} fifosupport_t;
//todo: include overflow indicator. After Overflow, no additional data is accepted until buffer is completely read and overflow is quit!


//Get number of elements currently in buffer
extern size_t fifosupport_numele(fifosupport_t* fifopara);
extern int fifosupport_getIdxWrite(fifosupport_t* fifopara, size_t* idx);
extern int fifosupport_commitWrite(fifosupport_t* fifopara);
extern int fifosupport_getIdxRead(fifosupport_t* fifopara, size_t* idx);
extern int fifosupport_freeOne(fifosupport_t* fifopara);
extern size_t fifosupport_lock(fifosupport_t* fifopara);
extern int fifosupport_getIdxLockRead(fifosupport_t* fifopara, size_t* idx, size_t offs);
extern int fifosupport_freeLocked(fifosupport_t* fifopara);


#endif
