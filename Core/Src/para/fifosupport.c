#include <fifosupport.h>

//todo: rename fifopara -> fifosupport

//Get number of elements currently in buffer
size_t fifosupport_numele(fifosupport_t* fifopara){
	if(!fifopara) return(0); //"0" means zero elements or parameter not available

	int tmp = (int)fifopara->pwrite - (int)fifopara->pread;
	if(tmp < 0) tmp += (int)fifopara->size;

	return(tmp);
}

//Get index of Element to write, returns -1 if FIFO is full
int fifosupport_getIdxWrite(fifosupport_t* fifopara, size_t* idx){
	//abort, if FIFO full
	if (fifopara->pwrite + 1 == fifopara->pread || (fifopara->pread == 0 && fifopara->pwrite + 1 == fifopara->size)){
		return -1; // fifo full
	}

	*idx = fifopara->pwrite;
	return(0);
}

//Commit write of one Element
int fifosupport_commitWrite(fifosupport_t* fifopara){
	if (fifopara->pwrite + 1 == fifopara->pread || (fifopara->pread == 0 && fifopara->pwrite + 1 == fifopara->size)){
		return -1; // fifo full
	}

	fifopara->pwrite += 1;
	if (fifopara->pwrite >= fifopara->size){
		fifopara->pwrite = 0;
	}

	return 0;
}

//*** Functions for single element read access ***

//Get index of Element to read, returns -1 if FIFO is empty
int fifosupport_getIdxRead(fifosupport_t* fifopara, size_t* idx){
	//abort, if FIFO empty
	if (fifopara->pread == fifopara->pwrite){
		return -1; // fifo empty
	}
	*idx = fifopara->pread;
	return(0);
}

//Free read Element
int fifosupport_freeOne(fifosupport_t* fifopara){
	if (fifopara->pread == fifopara->pwrite){
		return -1;  //fifo empty
	}

	fifopara->pread = fifopara->pread + 1;

	if (fifopara->pread >= fifopara->size){
		fifopara->pread = 0;
	}

	return 0;
}

//*** Functions for block / array read access ***

//Lock current numbers of elements for array read
size_t fifosupport_lock(fifosupport_t* fifopara){
	if(fifopara->numlock != 0){ //error; already locked!!
	}
	else {
		fifopara->numlock = fifosupport_numele(fifopara);
	}
	return(fifopara->numlock);
}

//Get index of locked element with number "offs"
int fifosupport_getIdxLockRead(fifosupport_t* fifopara, size_t* idx, size_t offs){
	if(offs > fifopara->numlock){
		return(-1); //access more than locked number of elements
	}
	*idx = fifopara->pread + offs;

	if (*idx >= fifopara->size){
		*idx -= fifopara->size;
	}
	return(0);
}

int fifosupport_freeLocked(fifosupport_t* fifopara){
	fifopara->pread += fifopara->numlock;
	if (fifopara->pread >= fifopara->size){
		fifopara->pread -= fifopara->size;
	}
	fifopara->numlock = 0;
	return(0);
}




