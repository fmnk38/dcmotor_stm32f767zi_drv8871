#include "parameter.h"
#include "fifoparameter.h"


int32_t val_para_mode = 0;
static parameter_t para_mode = {
		.type = parameterType_int32,
		.name = "Mode",
		.cnt = 1,
		.pdata = &val_para_mode
};


float val_para_uref[2] = {0.0f,10.0f};
static parameter_t para_uref = {
		.type = parameterType_float32,
		.name = "Ref. voltage",
		.cnt = 2,
		.pdata = &val_para_uref
};

float val_para_nref[2] = {0.0f,80.0f};
static parameter_t para_nref = {
		.type = parameterType_float32,
		.name = "Ref. Speed",
		.cnt = 2,
		.pdata = &val_para_nref
};

int32_t val_para_mode_dly = 0;
static parameter_t para_mode_dly = {
		.type = parameterType_int32,
		.name = "Mode Delay",
		.cnt = 1,
		.pdata = &val_para_mode_dly
};


int32_t val_para_datarecClkdiv = 3;
static parameter_t para_datarecClkdiv = {
		.type = parameterType_int32,
		.name = "Data Recorder clkdiv",
		.cnt = 1,
		.pdata = &val_para_datarecClkdiv
};

parameterList_t myList = {
		.name = "DC Motor control Parameter List",
		.cnt = 20,
		.parameters = {
				[0] = &para_mode,
				[1] = &para_uref,
				[2] = &para_nref,
				[3] = &para_mode_dly,
				[10] = &para_datarecClkdiv,
				[19] = 0
			}
};

extern fifoparameter_t datarec_current;
extern fifoparameter_t datarec_outpvolt;
extern fifoparameter_t datarec_spd;


fifoparameterList_t myFifoList = {
		.name = "Fifolist",
		.cnt = 3,
		.parameters = {
				[0] = &datarec_current,
				[1] = &datarec_outpvolt,
				[2] = &datarec_spd
		}
};


