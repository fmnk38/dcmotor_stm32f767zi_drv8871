#ifndef __PARAMETER_H
#define __PARAMETER_H

#include <stdlib.h>
#include <stdint.h>

#define ERR_NONE         0
#define ERR_OUTOFRANGE  -1
#define ERR_NOPARA      -2
#define ERR_PARSE       -3
#define ERR_TYPE        -4

typedef enum {
	parameterType_int32 = 1,
	parameterType_int16 = 2,
	parameterType_float32 = 10
} parameterType_t;

typedef struct  {
	parameterType_t type;
	char* name;
	size_t cnt;
	void* pdata;
} parameter_t;

typedef struct {
	size_t cnt;
	char* name;
	parameter_t* parameters[];
} parameterList_t;

extern int parameter_getName(char** s, parameter_t* para);
extern int parameter_toString(char* s, parameter_t* para, size_t element);

extern size_t parameter_listNumele(parameterList_t* paralist);
extern int parameter_listParaGetName(char** s, parameterList_t* paralist, size_t idx);
extern size_t parameter_listParaNumele(parameterList_t* paralist, size_t idx);
extern int parameter_listParaToString(char* s, parameterList_t* paralist, size_t idx, size_t element);
extern int parameter_listParaParseString(char* s, parameterList_t* paralist, size_t idx, size_t element);

#endif //__PARAMETER_H
