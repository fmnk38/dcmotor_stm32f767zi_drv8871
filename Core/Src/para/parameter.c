#include "parameter.h"
#include <math.h>
#include <string.h>



//https://stackoverflow.com/questions/2302969/convert-a-float-to-a-string
static double PRECISION = 0.00000000000001;
/**
 * Double to ASCII
 */
char * dtoa(char *s, double n) {
	// handle special cases
	if (isnan(n)) {
		strcpy(s, "nan");
	} else if (isinf(n)) {
		strcpy(s, "inf");
	} else if (n == 0.0) {
		strcpy(s, "0");
	} else {
		int digit, m, m1;
		char *c = s;
		int neg = (n < 0);
		if (neg)
			n = -n;
		// calculate magnitude
		m = log10(n);
		int useExp = (m >= 14 || (neg && m >= 9) || m <= -9);
		if (neg)
			*(c++) = '-';
		// set up for scientific notation
		if (useExp) {
			if (m < 0)
				m -= 1.0;
			n = n / pow(10.0, m);
			m1 = m;
			m = 0;
		}
		if (m < 1.0) {
			m = 0;
		}
		// convert the number
		while (n > PRECISION || m >= 0) {
			double weight = pow(10.0, m);
			if (weight > 0 && !isinf(weight)) {
				digit = floor(n / weight);
				n -= (digit * weight);
				*(c++) = '0' + digit;
			}
			if (m == 0 && n > 0)
				*(c++) = '.';
			m--;
		}
		if (useExp) {
			// convert the exponent
			int i, j;
			*(c++) = 'e';
			if (m1 > 0) {
				*(c++) = '+';
			} else {
				*(c++) = '-';
				m1 = -m1;
			}
			m = 0;
			while (m1 > 0) {
				*(c++) = '0' + m1 % 10;
				m1 /= 10;
				m++;
			}
			c -= m;
			for (i = 0, j = m-1; i<j; i++, j--) {
				// swap without temporary
				c[i] ^= c[j];
				c[j] ^= c[i];
				c[i] ^= c[j];
			}
			c += m;
		}
		*(c) = '\0';
	}
	return s;
}


int parameter_getName(char** s, parameter_t* para){
	if(!para) return(ERR_NOPARA);
	*s = para->name;
	return(0);
}

size_t parameter_numele(parameter_t* para){
	if(!para) return(0); //"0" means zero elements or parameter not available
	return(para->cnt);
}

static int32_t getIntVal(void* pdata, size_t element){
	int32_t* pdata_int32 = (int32_t*) pdata;
	int32_t val = pdata_int32[element];
	return(val);
}

static void setIntVal(void* pdata, size_t element, int32_t val){
	int32_t* pdata_int32 = (int32_t*) pdata;
	pdata_int32[element] = val;
}

static int16_t getInt16Val(void* pdata, size_t element){
	int16_t* pdata_int16 = (int16_t*) pdata;
	int16_t val = pdata_int16[element];
	return(val);
}

static void setInt16Val(void* pdata, size_t element, int16_t val){
	int16_t* pdata_int16 = (int16_t*) pdata;
	pdata_int16[element] = val;
}

static float getFloatVal(void* pdata, size_t element){
	float* pdata_float32 = (float*) pdata;
	float val = pdata_float32[element];
	return(val);
}

static void setFloatVal(void* pdata, size_t element, float val){
	float* pdata_float32 = (float*) pdata;
	pdata_float32[element] = val;
}

int parameter_toString(char* s, parameter_t* para, size_t element){
	if(!para) return(ERR_NOPARA);
	if(element >= para->cnt) return(ERR_OUTOFRANGE);
	parameterType_t type = para->type;
	if(type == parameterType_int32){
		int32_t val = getIntVal(para->pdata, element);
		itoa(val, s, 10);
	}
	else if(type == parameterType_int16){
		int16_t val = getInt16Val(para->pdata, element);
		itoa(val, s, 10);
	}
	else if(type == parameterType_float32){
		float val = getFloatVal(para->pdata, element);
		dtoa(s,val);
		return(ERR_TYPE);
	}
	else {
		return(ERR_TYPE);
	}
	return(ERR_NONE);
}

int parameter_parseString(char* s, parameter_t* para, size_t element){
	if(!para) return(ERR_NOPARA);
	if(element >= para->cnt) return(ERR_OUTOFRANGE);
	parameterType_t type = para->type;
	if(type == parameterType_int32){
		char * pEnd;
		int32_t val = strtol (s, &pEnd, 10);
		//todo: Check pEnd
		setIntVal(para->pdata, element, val);
	}
	else if(type == parameterType_int16){
		char * pEnd;
		int16_t val = (int16_t) strtol (s, &pEnd, 10);
		//todo: Check pEnd
		setInt16Val(para->pdata, element, val);
	}
	else if(type == parameterType_float32){
		char * pEnd;
		float val = strtof(s, &pEnd);
		//todo: Check pEnd
		setFloatVal(para->pdata, element, val);
	}
	else {
		return(ERR_TYPE);
	}
	return(ERR_NONE);
}

int parameter_listGetName(char** s, parameterList_t* paralist){
	return(ERR_NONE);
}


int parameter_listGetPara(parameter_t** para, size_t idx, parameterList_t* paralist){
	if(idx >= paralist->cnt) return(ERR_OUTOFRANGE);   //index out of range
	parameter_t* myPara = paralist->parameters[idx];
	*para = myPara;
	if(myPara == 0) return(ERR_NOPARA);   //parameter not available
	return(ERR_NONE);
}

size_t parameter_listNumele(parameterList_t* paralist){
	return(paralist->cnt);
}

size_t parameter_listParaNumele(parameterList_t* paralist, size_t idx){
	parameter_t* para;
	int ret = parameter_listGetPara(&para, idx, paralist);
	if(ret) return(ret);
	return(parameter_numele(para));
}

int parameter_listParaGetName(char** s, parameterList_t* paralist, size_t idx){
	parameter_t* para;
	int ret = parameter_listGetPara(&para, idx, paralist);
	if(ret) return(ret);
	return(parameter_getName(s, para));
}

int parameter_listParaToString(char* s, parameterList_t* paralist, size_t idx, size_t element){
	parameter_t* para;
	int ret = parameter_listGetPara(&para, idx, paralist);
	if(ret) return(ret);
	ret = parameter_toString(s, para, element);
	return(ret);
}

int parameter_listParaParseString(char* s, parameterList_t* paralist, size_t idx, size_t element){
	parameter_t* para;
	int ret = parameter_listGetPara(&para, idx, paralist);
	if(ret) return(ret);
	ret = parameter_parseString(s, para, element);
	return(ret);
}
