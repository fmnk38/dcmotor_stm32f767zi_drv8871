#ifndef __FIFOPARAMETER_H
#define __FIFOPARAMETER_H

#include "fifosupport.h"

typedef struct {
	parameter_t *parameter;
	fifosupport_t *fifosupport;
} fifoparameter_t;


typedef struct {
	size_t cnt;
	char* name;
	fifoparameter_t* parameters[];
} fifoparameterList_t;

extern int fifoparameter_listParaGetName(char** s, fifoparameterList_t* paralist, size_t idx);

extern size_t fifoparameter_listParaNumele(fifoparameterList_t* paralist, size_t idx);
extern int fifoparameter_listParaToString(char* s, fifoparameterList_t* paralist, size_t idx, size_t element);
extern int fifoparameter_listParaFreeLocked(fifoparameterList_t* paralist, size_t idx);


#endif
