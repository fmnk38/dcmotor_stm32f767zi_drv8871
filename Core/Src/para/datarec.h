#ifndef __DATAREC_H
#define __DATAREC_H

#include "parameter.h"
typedef struct  {
	parameterType_t type;
	char* name;
	void* pdata;
} datarec_signal_t;

typedef datarec_signal_t* pdatarec_signal_t;

typedef enum {
	datarec_triggerType_none = 0,
	datarec_triggerType_auto = 1,
	datarec_triggerType_rising = 2,
	datarec_triggerType_falling = 3,
	datarec_triggerType_either = 4
} datarec_triggerType_t;

typedef struct  {
	datarec_triggerType_t type;
	float level;
	datarec_signal_t* signal;
} datarec_trigger_t;


typedef struct {
	size_t noOfSignals;
	pdatarec_signal_t* signals; //Array of pointers to signals
	size_t buffersize; //total buffer size; max record length = buffersize/no of enabled signals
	float* pbuffer; //pointer to data buffer
	datarec_trigger_t trigger; //Trigger conditions and signal
} datarec_t;


#endif //__DATAREC_H
